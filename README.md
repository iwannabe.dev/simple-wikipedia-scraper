# Wikipedia Web Crawler and Scraper

This is a Python script that crawls and scrapes a given URL on wikipedia.org to search for the word "Hitler". If the word is not found on the initial page, the script continues searching on subpages from all the links provided on the initial URL until it finds a subpage where the word "Hitler" is mentioned. The script then outputs the number of nesting levels from the starting URL to the final URL where the word was found.

## Installation

1. Make sure you have Python 3 installed on your system. You can check by running `python3 --version` in your terminal.

2. Clone the repository or download the script file.

3. Install the required dependencies by running the following command in your terminal:

   ```shell
   pip install requests beautifulsoup4
   ```

## Usage

To use the script, follow these steps:

1. Open your terminal and navigate to the directory where the script is located.

2. Run the script using the following command:

   ```shell
   python3 wiki_scraper.py [URL]
   ```

   Replace `[URL]` with the Wikipedia URL you want to start the crawling and scraping from. If no URL is provided, the script will use a default URL as an example.

3. Wait for the script to finish crawling and scraping. The script will print the progress and update you on the nesting level and pages visited.

4. If the word "Hitler" is found on a subpage, the script will display a message with the nesting level and the final URL where the word was found.

## Example

```shell
python3 wiki_scraper.py https://en.wikipedia.org/wiki/Yellow-throated_miner
```

Output:

```
Checking: https://en.wikipedia.org/wiki/Yellow-throated_miner (Nesting level: 1, Pages visited: 1)
Checking: https://en.wikipedia.org/wiki/Honeyeater (Nesting level: 2, Pages visited: 40)
Checking: https://en.wikipedia.org/wiki/Australia (Nesting level: 3, Pages visited: 86)
We have found a mention about Hitler on nesting level 4 after visiting 104 URLs! Final URL was: https://en.wikipedia.org/wiki/Australia
```

## Note

The script is implemented to search for the word "Hitler" in a case-insensitive manner. If you want to search for a different word, you can modify the code by changing the value of the `Hitler` string literal in the `has_hitler_been_mentioned` method.
